//
//  SDViewController.m
//  SVGDemo
//
//  Created by sodas on 4/22/14.
//
/*
 Copyright 2014 sodas tsai
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

#import "SDViewController.h"
#import "SIImage.h"

@implementation SDViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"lightning" ofType:@"svg"];
    SIImage *svgImage = [SIImage imageWithContentsOfFile:path];
    CFTimeInterval t = CACurrentMediaTime();
    svgImage.scale = 3.0;
    UIImage *image = [svgImage UIImage];
    NSLog(@"First Rendered: %.3lf secs", CACurrentMediaTime() - t);
    
    self.imageView.image = image;
    
    t = CACurrentMediaTime();
    svgImage.scale = 2.0;
    image = [svgImage UIImage];
    NSLog(@"Second Rendered: %.3lf secs", CACurrentMediaTime() - t);
    
    NSLog(@"-----------------");
    
    dispatch_async(dispatch_get_main_queue(), ^{
        CFTimeInterval totalTimeInterval = 0;
        NSInteger times = 200;
        for (NSInteger i=0; i<times; ++i) {
            CFTimeInterval start = CACurrentMediaTime();
            [[SIImage imageWithContentsOfFile:path] UIImage];
            totalTimeInterval += CACurrentMediaTime() - start;
        }
        CFTimeInterval averageTimeInterval = totalTimeInterval/times;
        NSLog(@"%@: %.3lf sec (%ld times)", [path lastPathComponent], averageTimeInterval, (long)times);
    });
}

@end
