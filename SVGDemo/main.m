//
//  main.m
//  SVGDemo
//
//  Created by sodas on 4/22/14.
//  Copyright (c) 2014 sodastsai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SDAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SDAppDelegate class]));
    }
}
