Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.name                = "SVGImage"
  s.version             = "0.0.4"
  s.summary             = "Use WebKit to render SVG images"
  s.homepage            = "https://bitbucket.org/sodastsai/svgimage"

  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.license             = "Apache License 2.0"

  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.author              = { "sodastsai" => "sodas2002@gmail.com" }

  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.platform            = :ios, "7.0"

  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.source              = {
    :git => "https://bitbucket.org/sodastsai/svgimage",
    :tag => "v0.0.4"
  }

  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.source_files        = "SVGImage/**/*.{h,m}"
  s.public_header_files = "SVGImage/**/*.{h,m}"

  # ――― Resources ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.frameworks          = "UIKit", "CoreGraphics", "QuartzCore", "JavaScriptCore"

  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.requires_arc        = true

end
